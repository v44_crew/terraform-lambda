
variable "iam_role_name_prefix" {
  description = "The prefix string for the name of IAM role for the lambda function."
  default     = ""
}
variable "tags" {
  default     = {}
  description = "Map of tags for Lambda function"
  type        = "map"
}

variable "policy_arns" {
  description = "A list of IAM policy ARNs attached to the lambda function."
  default     = []
}

variable "retention_in_days" {
  description = "Specifies the number of days you want to retain log events in the specified log group."
  default     = "null"
}

variable "kms_key_id" {
  description = "The ARN of the KMS Key to use when encrypting log data."
  type        = "string"
  default     = "null"
}

variable "runtime" {
  description = "The identifier of the function's runtime."
}

variable "handler" {
  description = "The function entrypoint in your code."
}

variable "description" {
  description = "Description of what your Lambda Function does."
  default     = ""
}

variable "function_name" {
  description = "A unique name for your Lambda Function."
}

variable "memory_size" {
  description = "Amount of memory in MB your Lambda Function can use at runtime."
  default     = 128
}

variable "reserved_concurrent_executions" {
  description = "The amount of reserved concurrent executions for this lambda function. A value of 0 disables lambda from being triggered and -1 removes any concurrency limitations."
  default     = -1
}

variable "layers" {
  description = "List of Lambda Layer Version ARNs (maximum of 5) to attach to your Lambda Function."
  default     = []
}

variable "timeout" {
  description = "The maximum number of seconds the lambda function to run until timeout."
  default     = 3
}

variable "publish" {
  description = "Whether to publish creation/change as new Lambda Function Version."
  default     = false
}

variable "environment" {
  default     = {}
  description = "Map of environment variables for Lambda function"
  type        = "map"
}

variable "build_command" {
  description = "This is the build command to execute. It can be provided as a relative path to the current working directory or as an absolute path. It is evaluated in a shell, and can use environment variables or Terraform variables."
  default     = ""
}

variable "build_triggers" {
  description = "A map of values which should cause the build command to re-run. Values are meant to be interpolated references to variables or attributes of other resources."
  default     = []
}

variable "source_dir" {
  description = "A path to the directory which contains source files."
}

variable "output_path" {
  description = "A path to which the source directory is archived before uploading to AWS."
}

variable "dead_letter_config" {
  description = "The ARM of the function's dead letter queue (either SQS queue or SNS topic)."
  type = "string"
}

variable "tracing_config" {
  description = "Can be either PassThrough or Active. If PassThrough, Lambda will only trace the request from an upstream service if it contains a tracing header with \"sampled=1\". If Active, Lambda will respect any tracing header it receives from an upstream service. If no tracing header is received, Lambda will call X-Ray for a tracing decision."
  type = "string"
  default = "null"
}

variable "subnet_ids" {
  description = "List of subnet IDs for Lambda VPC config (leave empty if no VPC)"
  type        = "list"
  default     = []
}

variable "security_group_ids" {
  description = "List of security group IDs for Lambda VPC config (leave empty if no VPC)"
  type        = "list"
  default     = []
}