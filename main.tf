resource "aws_iam_role" "this" {
  name_prefix = "${var.iam_role_name_prefix}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  tags = "${var.tags}"
}

resource "aws_iam_role_policy_attachment" "basic" {
  role       = "${aws_iam_role.this.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda" {
  count = "${length(var.policy_arns)}"

  role       = "${aws_iam_role.this.name}"
  policy_arn = "${var.policy_arns[count.index]}"
}

resource "aws_cloudwatch_log_group" "this" {
  name              = "/aws/lambda/${aws_lambda_function.this.function_name}"
  retention_in_days = "${var.retention_in_days}"
  kms_key_id        = "${var.kms_key_id}"

  tags = "${var.tags}"
}

resource "aws_lambda_function" "this" {
  filename         = "${data.archive_file.source.output_path}"
  role             = "${aws_iam_role.this.arn}"
  source_code_hash = "${data.archive_file.source.output_base64sha256}"

  runtime                        = "${var.runtime}"
  handler                        = "${var.handler}"
  description                    = "${var.description}"
  function_name                  = "${var.function_name}"
  memory_size                    = "${var.memory_size}"
  reserved_concurrent_executions = "${var.reserved_concurrent_executions}"
  layers                         = "${var.layers}"
  timeout                        = "${var.timeout}"
  publish                        = "${var.publish}"
  dead_letter_config             = "${var.dead_letter_config}"
  tracing_config                 = "${var.tracing_config}"

  vpc_config {
    subnet_ids         = "${var.subnet_ids}"
    security_group_ids = "${var.security_group_ids}"
  }

  environment {
    variables = "${var.environment}"
  }

  tags = "${var.tags}"
}